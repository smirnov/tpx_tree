Analysis source code for the ATLAS TRT test beam studies with TimePix3 telescope in June-July 2017 at SPS H8 beamline.

Compilation:  
  make tpx_tree  
  - tested on lxplus SLC6.9 with g++ vers. 4.9.3 and ROOT 6.06  
    and on MacOS Sierra vers. 10.12 with c++ 8.1 and ROOT 6.10

Running:  
  ./tpx_tree_slc -r NN  for linux or  
  ./tpx_tree_osx -r NN  for MacOS  
  - NN - run number to process (e.g.: ./tpx_tree_slc -r 24)

Input raw data:  
  all 4 planes of TimePix3 telescope for a given run from files located at /eos/atlas/atlascerngroupdisk/det-trt-tb/testbeam2017/TimePix/RawData/

Output ROOT file:  
  ROOT file 'runNN.root' with two trees: 'tpx_trig' and 'tpx_data'

  Tree 'tpx_trig' contains branch 'triggers' with the following variables:  
    time25ns - trigger global timestamp in 25 ns units  
    cntr - trigger counter (12 bit)  
    time3ns - trigger global additional fine timestamp in 3.125 ns units (3 bit)  
    stamp - additional 4 bits of 320 MHz trigger time stamp  
    dev - TimePix device number which has the trigger (devices 0 and 1 correspond to 'pion' trigger, devices 2 and 3 - to 'electron')

  Tree 'tpx_data' contains branch 'pixels' with the following variables:  
    time25ns - pixel hit global timing in 25 ns units (ToA)  
    ToT - pixel hit time-over-thresold  
    time1_56ns - pixel hit additional precise 640 MHz 4-bit timing (Fast ToA)  
    X - pixel hit x-coordinate (column number 0...255)  
    Y - pixel hit y-coordinate (row number 0...255)  
    dev - TimePix plane number of the hit (0...3)
