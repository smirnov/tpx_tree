#
platform = $(shell uname -s)
#CXX =
CXXFLAGS = -std=c++11
ifeq ($(platform),Darwin)
  suffix=osx
endif
ifeq ($(platform),Linux)
  suffix=slc
endif
HEADREVISION = $(shell git describe --tag)
#
tpx_tree:	tpx_tree.cpp tpx_tree.h
		$(CXX) $(shell root-config --cflags --libs) -DMYREVISION=\"$(HEADREVISION)\" -o $@_$(suffix) tpx_tree.cpp
