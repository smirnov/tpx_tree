//Root-tree branch "pixels" contents:
struct TPXpixel_t {
  ULong64_t time25ns;
  UShort_t  ToT;
  UChar_t   time1_56ns;
  UChar_t   X;
  UChar_t   Y;
  UChar_t   dev;
};

//Root-tree branch "triggers" contents:
struct TPXtrigger_t {
  ULong64_t time25ns;
  UShort_t  cntr;
  UChar_t   time3ns;
  UChar_t   stamp;
  UChar_t   dev;
};
