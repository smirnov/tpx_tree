// code to read June-July 2017 TimePix3 raw data files and output "pass 0" ROOT trees
#include <iostream>
#include <map>
#include <array>
#include <unistd.h>  //getopt()
#include <sstream>   //stringstream

#include "TTree.h"
#include "TFile.h"
#include "TH1.h"

#include "tpx_tree.h"

using namespace std;

int main(int argc, char** argv)
{
  const uint16_t MAXDEV = 4;
  const string revision(MYREVISION);
  map<uint32_t, string> Runs =
    {{18, "170629-001917"}, {19, "170629-002909"},
     {20, "170629-005154"}, {21, "170629-011505"},
     {22, "170629-013708"}, {23, "170629-021006"},
     {24, "170629-023145"}, {25, "170629-025038"},
     {26, "170629-031322"}, {27, "170629-032957"},
     {28, "170629-035551"}, {29, "170629-041515"},
     {30, "170629-043444"}, {31, "170629-050102"},
     {32, "170629-053546"}, {33, "170629-060559"},
     {34, "170629-062954"}, {35, "170629-065749"},
     {36, "170629-072751"}, {49, "170630-172116"},
     {50, "170630-180422"}, {51, "170630-183403"},
     {52, "170630-185502"}, {53, "170630-191934"},
     {54, "170630-194316"}, {55, "170630-200546"},
     {56, "170630-201753"}, {57, "170630-203027"},
     {58, "170630-205007"}, {59, "170630-210138"},
     {60, "170630-211659"}, {61, "170630-212856"},
     {62, "170630-214805"}, {63, "170630-221151"},
     {64, "170630-224105"}, {65, "170630-230309"},
     {66, "170630-232641"}, {67, "170703-083006"},
     {68, "170703-085806"}, {69, "170703-093339"},
     {70, "170703-100421"}, {71, "170703-103811"},
     {72, "170703-112828"}, {73, "170703-120915"},
     {74, "170703-123549"}, {76, "170703-133003"},
     {77, "170703-144515"}, {78, "170703-150839"},
     {79, "170703-154846"}, {80, "170703-160708"},
     {81, "170703-163413"}
    };

  map<uint32_t, string> devindx =
    {{0, "W0002_H07"},
     {1, "W0002_I03"},
     {2, "W0002_G05"},
     {3, "W0005_J04"}};

  int32_t c;
  uint32_t runnumber(0);
  while ((c=getopt(argc, argv, "hr:v")) != -1) {
    switch(c) {
      case 'h':
        cout << "  -h    - print this help and exit" << endl;
        cout << "  -r NN - process run number NN" << endl;
        cout << "  -v    - print version number and exit" << endl;
        return 0;
      case 'r':
        runnumber = atoi(optarg);
        if(Runs.count(runnumber) == 0) {
          cout << "Error! Illegal run number: " << runnumber << endl;
          return 1;
        }
        break;
      case 'v':
        cout << "tpx_tree " << revision << endl;
        return 0;
      default:
        cout << argv[0] << ":  use -h option for help" << endl;
        return 1;
    }
  }
  string filepath("/eos/atlas/atlascerngroupdisk/det-trt-tb/testbeam2017/TimePix/DATA/");
  //string filepath("../testbeam/");    //<-- for laptop
  if(runnumber == 0) {
    cout << "Run number is not specified !" << endl;
    cout << "Enter the run number: ";
    cin >> runnumber;
  }

  stringstream rf;
  rf << "run" << runnumber;
  string rootfilename = rf.str() + ".root";

  TFile *f = new TFile(rootfilename.c_str(), "RECREATE");
  if (f == NULL)
    { cout << "ERROR: could not open Root file " << rootfilename << endl; return -1; }
  f->cd();

  TH1F *hTPXdiff  = new TH1F("hTPXdiff","Interval between global timer stamps (sec)",5000,1.05,1.15);
  TH1F *hTtrig    = new TH1F("hTtrig","Trigger arrival time (sec)",25000,0,2500);
  TH1F *hSpillLen = new TH1F("hSpillLen","Spill length (sec)",800,0,8);
  TH1F *hSpillInt = new TH1F("hSpillInt","Interval between spills (sec)",500,0,50);

//arrays to store trigger and pixel data for each long timestamp
  typedef std::array<uint16_t, 4> trigdata_t;
  trigdata_t trigdata;
  typedef std::array<uint16_t, 5> pixdata_t;
  pixdata_t pixdata;

//maps to store all long timestamps with corresponding trigger and pixel data arrays
  map<uint64_t, trigdata_t> trig_map;
  trig_map.clear();
  multimap<uint64_t, pixdata_t> pix_map;
  pix_map.clear();

  for(uint16_t dev=0; dev<MAXDEV; dev++)
  {
    stringstream ss;
    ss << filepath << "Dev" << dev << "/Run" << runnumber << "/" << devindx[dev];
    ss << "-" << Runs[runnumber] << "-" << runnumber;
    string fname = ss.str() + "-1.dat";
    FILE *fp = fopen(fname.c_str(), "r");
    if (fp == NULL) {
      string date_time = Runs[runnumber];
      size_t k = date_time.length();
      if(date_time[k-1] == '9') {
        if(date_time[k-2] == '5') {
          date_time[k-3] += 1;
          date_time[k-2] = '0';
          date_time[k-1] = '0';
        }
        else {
          date_time[k-2] += 1;
          date_time[k-1] = '0';
        }
      }
      else
        date_time[k-1] += 1;
      stringstream ss1;
      ss1 << filepath << "Dev" << dev << "/Run" << runnumber << "/" << devindx[dev];
      ss1 << "-" << date_time << "-" << runnumber;
      fname = ss1.str() + "-1.dat";
      fp = fopen(fname.c_str(), "r");
      if (fp == NULL)
      { cout << "ERROR: cannot open file: " << fname << endl; f->Write(); f->Close(); return -1; }
    }
    cout << fname << endl;

    int32_t  rc;
    uint32_t sphdr_id;
    uint32_t sphdr_size;
    rc = fread( &sphdr_id,   sizeof(uint32_t), 1, fp);
    rc = fread( &sphdr_size, sizeof(uint32_t), 1, fp);

    if(sphdr_id != 0x52445053) {
      cout << "ERROR: invalid SPIDR identifier: 0x" << hex << sphdr_id << dec << endl;
      fclose(fp);
      f->Write();
      f->Close();
      return -1;
    }

    cout << "header size " << sphdr_size << endl;
    if (sphdr_size > 66304) sphdr_size = 66304;
    uint32_t *fullheader = new uint32_t[sphdr_size/sizeof(uint32_t)];
    if (fullheader == 0)
      { cout << "ERROR: failed to allocate memory for header" << endl; f->Write(); f->Close(); return -1; }

    rc = fread ( fullheader+2, sizeof(UInt_t), sphdr_size/sizeof(UInt_t) -2, fp);
    fullheader[0] = sphdr_id;
    fullheader[1] = sphdr_size;

    uint32_t header;
    uint32_t pcntr = 0;
    uint32_t hdr=0;
    map<uint32_t, uint32_t> headers;
    headers.clear();

    uint64_t tpxpacket=0;
    uint32_t longtime_lsb=0;
    uint64_t longtime_msb=0;
    uint64_t longtime_prev=0;
    uint64_t longtime=0;
    int64_t interval=0;
    bool msb_flag=false;
    bool lsb_flag=false;

    uint64_t time25ns=0;
    uint32_t ToA=0;
    uint32_t SPIDRtime=0;
    uint8_t  dcol=0;
    uint8_t  spix=0;
    uint8_t  pix=0;

//read the raw data files and fill trigger and pixel maps (trig_map and pix_map)
//trigger map is sorted by global time
    while (!feof(fp) ) {
      if( fread( &tpxpacket, sizeof(uint64_t), 1, fp) != 1) continue;
      pcntr++;
      header = (tpxpacket & 0xFF00000000000000) >> 56;
      hdr = (header & 0xF0) >> 4;
      if(hdr == 0xB)
        header = hdr;
      headers[header]++;

      // pixel data packet
      if(header == 0xB) {
        ToA       =(tpxpacket & 0x00000FFFC0000000) >> 30;
        SPIDRtime = tpxpacket & 0x000000000000FFFF;
        time25ns  = (SPIDRtime << 14) | ToA;
        if(longtime > 0)
          time25ns = (longtime & 0x0000FFFFC0000000) | time25ns;
        dcol       = (tpxpacket & 0x0FE0000000000000) >> 53;
        spix       = (tpxpacket & 0x001F800000000000) >> 47;
        pix        = (tpxpacket & 0x0000700000000000) >> 44;
        pixdata[0] = (tpxpacket & 0x000000003FF00000) >> 20; //ToT
        pixdata[1] = (tpxpacket & 0x00000000000F0000) >> 16; //time1_56ns
        pixdata[2] = 2*dcol + pix/4;                         //X or column
        pixdata[3] = 4*spix +(pix & 0x3);                    //Y or row
        pixdata[4] = dev;                                    //Z or device
        pix_map.insert (std::make_pair(time25ns,pixdata));
      }

      // TPX3 timer packet LSBs (0x44) and MSBs (0x45)
      if(header == 0x44 || header == 0x45) {
        if(header == 0x44) {
          longtime_lsb = (tpxpacket & 0x0000FFFFFFFF0000) >> 16;
          lsb_flag=true;
        }
        if(header == 0x45) {
          longtime_msb = (tpxpacket & 0x00000000FFFF0000) >> 16;
          msb_flag=true;
        }
        if(lsb_flag && msb_flag) {
          longtime = (longtime_msb << 32) | longtime_lsb;
          lsb_flag = false;
          msb_flag = false;
          //cout << "          longtime= 0x" << hex << longtime << " = " << dec << longtime << endl;
          interval = (int64_t)longtime - (int64_t)longtime_prev;
          if( (interval >= 0x100000000) && (longtime_prev > 0) ) {
            longtime_msb--;
            longtime = (longtime_msb << 32) | longtime_lsb;
            interval = (int64_t)longtime - (int64_t)longtime_prev;
          }
          if(longtime_prev > 0)
            hTPXdiff->Fill(interval*25e-9);
          longtime_prev = longtime;
        }
      }

      // trigger packet
      if(header == 0x6F) {
        trigdata[0] = (tpxpacket & 0x00FFF00000000000) >> 44;   //cntr
        trigdata[1] = (tpxpacket & 0x0000000000000E00) >> 9;    //time 3 ns
        trigdata[2] = (tpxpacket & 0x00000000000001E0) >> 5;    //stamp
        time25ns    = (tpxpacket & 0x00000FFFFFFFF000) >> 12;
        if(longtime > 0)
          time25ns = (longtime & 0x0000FFFF00000000) | time25ns;
        trigdata[3] = dev;
        trig_map[time25ns] = trigdata;
      }
    }  // end while
    fclose(fp);
    for(auto i : headers) {
      cout << "header: 0x" << hex << i.first << ":  " << dec << i.second << endl;
    }
    cout << "dev=" << dev << "  pcntr=" << pcntr << endl << endl;;
  } //for(dev=

  TPXtrigger_t trigger;
  TPXpixel_t pixel;

  TTree *tpx_trig = new TTree("tpx_trig", "TimePix3 telescope: triggers, June-July 2017");
  TTree *tpx_data = new TTree("tpx_data", "TimePix3 telescope: pixel data, June-July 2017");
  tpx_trig->Branch("triggers",&trigger.time25ns,"time25ns/l:cntr/s:time3ns/b:stamp/b:dev/b");
  tpx_data->Branch("pixels",&pixel.time25ns,"time25ns/l:ToT/s:time1_56ns/b:X/b:Y/b:dev/b");

  uint32_t spill_cntr = 0;
  double t_trig;
  double t_trig_prev = 0;
  double start_spill = 0;
  double end_spill   = 0;
  uint64_t trigtime_prev = 0;
  bool start_of_run = true;
//read trigger and pixel maps and fill the corresponding Root-trees
  for(auto i : trig_map) {
    trigger.time25ns = i.first;
    trigger.cntr     = i.second[0];
    trigger.time3ns  = i.second[1];
    trigger.stamp    = i.second[2];
    trigger.dev      = i.second[3];
    tpx_trig->Fill();

//fill trigger monitoring histogramms for Dev3
    t_trig = trigger.time25ns * 25e-9;
    if(trigger.dev == 3) {
      hTtrig->Fill(t_trig);
      if(trigger.cntr == 1 && start_of_run) {
        trigtime_prev = 0;
        start_of_run = false;
      }
      if(t_trig - t_trig_prev > 1.0) {
        if(spill_cntr > 0) {
          end_spill = t_trig_prev;
          hSpillLen->Fill(end_spill - start_spill);
          hSpillInt->Fill(t_trig - start_spill);
        }
        start_spill = t_trig;
        spill_cntr++;
      }
      t_trig_prev = t_trig;
    }
//    cout << i.first << ": " << setw(6) << i.second[0] << setw(6) << i.second[1];
//    cout << setw(6) << i.second[2] << setw(6) << i.second[3] << endl;
  }
// last spill
  hSpillLen->Fill(t_trig_prev - start_spill);

  for(auto i : pix_map) {
    pixel.time25ns  = i.first;
    pixel.ToT       = i.second[0];
    pixel.time1_56ns= i.second[1];
    pixel.X         = i.second[2];
    pixel.Y         = i.second[3];
    pixel.dev       = i.second[4];
    tpx_data->Fill();
  }
  cout << "trig_map.size()=" << trig_map.size() << endl;
  cout << "pix_map.size()="  << pix_map.size()  << endl;

  f->cd();
  f->Write();
  f->Close();
  return 0;
}
